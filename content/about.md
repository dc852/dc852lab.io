+++
date = "2017-08-23"
title = "About"
+++

# About DC852
DEF CON Hong Kong (DC852) endeavors to provide in Hong Kong an open platform for ethical discussions of technology and security topics for the betterment of human society.

# Officers as of 2017

* Panda Lover: Lindsay (Ling Ling)
* BOFH: Albert Hui
* Research Officer: Alan Chung
* Research Officer: Dan Kelly
* Research Officer: Kenneth Tse
* Research Officer: Jay

# Founders
DC852 was founded on 2017-07-22 by:

1. Lindsay (Ling Ling)
1. Albert Hui
1. Alan Chung
1. Dan Kelly
1. Kenneth Tse
1. Jay
